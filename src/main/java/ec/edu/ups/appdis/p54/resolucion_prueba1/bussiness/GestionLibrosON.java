package ec.edu.ups.appdis.p54.resolucion_prueba1.bussiness;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ec.edu.ups.appdis.p54.resolucion_prueba1.dao.AutorDAO;
import ec.edu.ups.appdis.p54.resolucion_prueba1.dao.LibroDAO;
import ec.edu.ups.appdis.p54.resolucion_prueba1.modelo.Autor;
import ec.edu.ups.appdis.p54.resolucion_prueba1.modelo.Libro;

@Stateless
public class GestionLibrosON {

	@Inject
	private LibroDAO dao;
	
	@Inject
	private AutorDAO autorDAO;
	
	public void guardar(Libro libro) throws Exception {
		try {
			dao.insert(libro);
		}catch(Exception e) {
			throw new Exception("Error al insertar");
		}
		
	}
	
	public Autor getAutor(int codigo) {
		return autorDAO.read(codigo);
	}
	
	public List<Autor> getAutores(){
		return autorDAO.getAutoresConLibros();
	}
}
