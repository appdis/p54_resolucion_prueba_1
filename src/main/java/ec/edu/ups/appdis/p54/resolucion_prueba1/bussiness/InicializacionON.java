package ec.edu.ups.appdis.p54.resolucion_prueba1.bussiness;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import ec.edu.ups.appdis.p54.resolucion_prueba1.dao.AutorDAO;
import ec.edu.ups.appdis.p54.resolucion_prueba1.modelo.Autor;



@Startup
@Singleton
public class InicializacionON {
	
	@Inject
	private AutorDAO dao;
	
	
	@PostConstruct
	public void init() {
		
		System.out.println("inicializandoooooooooooooooooo");
		
		List<Autor> telefonos = dao.getAutores();
		if(telefonos.size()==0) {
			Autor tt1 = new Autor();
			tt1.setCodigo(1);
			tt1.setNombre("Juan");
			dao.insert(tt1);
			
			
			Autor tt2 = new Autor();
			tt2.setCodigo(2);
			tt2.setNombre("Pedro");
			dao.insert(tt2);
			
			Autor tt3 = new Autor();
			tt3.setCodigo(3);
			tt3.setNombre("Mario");
			dao.insert(tt3);
		}
		
		
	}

}
