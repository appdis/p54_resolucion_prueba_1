package ec.edu.ups.appdis.p54.resolucion_prueba1.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import ec.edu.ups.appdis.p54.resolucion_prueba1.modelo.Autor;

@Stateless
public class AutorDAO {

	@Inject
	private EntityManager em;
	
	public Autor read(int codigo) {
		return em.find(Autor.class, codigo);
	}
	
	public void insert(Autor autor) {
		em.persist(autor);
	}
	
	public List<Autor> getAutores(){
		String jpql = "SELECT p FROM Autor p ";
		
		Query q = em.createQuery(jpql, Autor.class);
		
		List<Autor> autores = q.getResultList();
		return autores;
	}
	
	public List<Autor> getAutoresConLibros(){
		String jpql = "SELECT p FROM Autor p ";
		
		Query q = em.createQuery(jpql, Autor.class);
		
		List<Autor> autores = q.getResultList();
		for(Autor autor: autores) {
			autor.getLibros().size();
		}
		return autores;
	}
	
	
}
