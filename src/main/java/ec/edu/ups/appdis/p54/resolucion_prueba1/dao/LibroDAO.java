package ec.edu.ups.appdis.p54.resolucion_prueba1.dao;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import ec.edu.ups.appdis.p54.resolucion_prueba1.modelo.Libro;

@Stateless
public class LibroDAO {

	@Inject
	private EntityManager em;
	
	public void insert(Libro libro) {
		em.persist(libro);
	}
}
