package ec.edu.ups.appdis.p54.resolucion_prueba1.modelo;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Autor {

	@Id
	private int codigo;
	private String nombre;
	
	@OneToMany
	@JoinColumn(name="autor_codigo")
	private List<AutorLibro> libros;
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public List<AutorLibro> getLibros() {
		return libros;
	}
	public void setLibros(List<AutorLibro> libros) {
		this.libros = libros;
	}
	
	
	
}
