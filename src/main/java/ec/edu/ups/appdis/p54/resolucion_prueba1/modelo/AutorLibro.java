package ec.edu.ups.appdis.p54.resolucion_prueba1.modelo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

@Entity
public class AutorLibro {

	@Id
	private int codigo;
	
	@OneToOne
	@JoinColumn(name="autor_codigo")
	private Autor autor;
	
	@OneToOne
	@JoinColumn(name="libro_codigo")
	private Libro libro;
	
	@Transient
	private int codigoAutorTemp; 

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public Autor getAutor() {
		return autor;
	}

	public void setAutor(Autor autor) {
		this.autor = autor;
	}

	public int getCodigoAutorTemp() {
		return codigoAutorTemp;
	}

	public void setCodigoAutorTemp(int codigoAutorTemp) {
		this.codigoAutorTemp = codigoAutorTemp;
	}

	public Libro getLibro() {
		return libro;
	}

	public void setLibro(Libro libro) {
		this.libro = libro;
	}
	
	
	
	
	
}
