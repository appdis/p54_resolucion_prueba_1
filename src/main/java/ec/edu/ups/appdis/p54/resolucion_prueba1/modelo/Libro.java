package ec.edu.ups.appdis.p54.resolucion_prueba1.modelo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Libro {

	@Id
	private int codigo; 
	private String nombre;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name="libro_codigo")
	private List<AutorLibro> autores;
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public List<AutorLibro> getAutores() {
		return autores;
	}
	public void setAutores(List<AutorLibro> autores) {
		this.autores = autores;
	}
	public void addAutorLibro(AutorLibro autorlibro) {
		if(autores==null)
			autores = new ArrayList<AutorLibro>();
		
		autores.add(autorlibro);
	}
	
}
