package ec.edu.ups.appdis.p54.resolucion_prueba1.view;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

import ec.edu.ups.appdis.p54.resolucion_prueba1.bussiness.GestionLibrosON;
import ec.edu.ups.appdis.p54.resolucion_prueba1.modelo.Autor;

@ManagedBean
public class AuroresController {

	public List<Autor> autores;
	
	@Inject
	private GestionLibrosON gON;
	
	@PostConstruct
	public void init() {
		autores =  gON.getAutores();
	}

	public List<Autor> getAutores() {
		return autores;
	}

	public void setAutores(List<Autor> autores) {
		this.autores = autores;
	}
	
	
}
