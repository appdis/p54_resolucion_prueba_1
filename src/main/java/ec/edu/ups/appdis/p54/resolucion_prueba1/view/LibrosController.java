package ec.edu.ups.appdis.p54.resolucion_prueba1.view;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import ec.edu.ups.appdis.p54.resolucion_prueba1.bussiness.GestionLibrosON;
import ec.edu.ups.appdis.p54.resolucion_prueba1.modelo.Autor;
import ec.edu.ups.appdis.p54.resolucion_prueba1.modelo.AutorLibro;
import ec.edu.ups.appdis.p54.resolucion_prueba1.modelo.Libro;

@ManagedBean
@ViewScoped
public class LibrosController {

	private Libro libro;
	
	@Inject
	private GestionLibrosON gON;
	
	
	@PostConstruct
	public void init() {
		libro = new Libro();
	}


	public Libro getLibro() {
		return libro;
	}


	public void setLibro(Libro libro) {
		this.libro = libro;
	}
	
	public String guardar() {
		
		try {
			gON.guardar(libro);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public void buscarAutor(AutorLibro autorLibro) {
		
		Autor autor = gON.getAutor(autorLibro.getCodigoAutorTemp());
		if(autor!=null) {
			autorLibro.setAutor(autor);
		}else {
			autorLibro.setAutor(null);
		}
		
		
	}
	
	public String addAutor() {
		libro.addAutorLibro(new AutorLibro());
		return null;
	}
}
